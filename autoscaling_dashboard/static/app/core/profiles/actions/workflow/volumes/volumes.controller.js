/*
 * Copyright 2015 IBM Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  /**
   * @ngdoc controller
   * @name CreateProfileVolumesController
   * @description
   * The `CreateProfileVolumesController` controller provides functions for
   * configuring the source step of the Launch Instance Wizard.
   *
   */
  var push = [].push;

  angular
    .module('horizon.cluster.profiles.actions')
    .controller('CreateProfileVolumesController', CreateProfileVolumesController);

  CreateProfileVolumesController.$inject = ['$scope'];

  function CreateProfileVolumesController($scope) {

    var ctrl = this;
    $scope.volumeTypeError = ""

    // toggle button label/value defaults
    ctrl.toggleButtonOptions = [
      { label: gettext('Yes'), value: true },
      { label: gettext('No'), value: false }
    ];

    ctrl.addVolumeDataDisk = function(spec){
      if (spec.volume_type === undefined || spec.volume_type === ""){
        $scope.volumeTypeError = 'Invalid Volume Type'
      } else {
        $scope.model.newProfileSpec.dataDisks.push(angular.copy(spec))
      }
    }

    ctrl.removeItem = function (index) {
      $scope.model.newProfileSpec.dataDisks.splice(index, 1);
    }
  }
})();
